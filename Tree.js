
items = [
  { id: 1, parent: "root", type: null },
  { id: 2, parent: 1, type: "test" },
  { id: 3, parent: 1, type: "test" },
  { id: 4, parent: 2, type: "test" },
  { id: 5, parent: 2, type: "test" },
  { id: 6, parent: 2, type: "test" },
  { id: 7, parent: 4, type: null },
  { id: 8, parent: 4, type: null },
];

class Tree  {

  constructor(items) {
    this.items = items;
  }

  getAll() {
    return this.items;
  }

  getItem(id)  {
    return this.items.find((item) => item.id === id);
  }

  getChildren(id) {
    return this.items.filter((item) => item.parent === id);
  }

  getAllChildren(id) {
    const children = this.getChildren(id);
    return children.concat(
      children.reduce((acc, item) => {
        const children = this.getAllChildren(item.id);
        return acc.concat(children);
      }, [])
    );
  }

  getAllParents(id) {
    const item = this.getItem(id);
    if (!item) {
      return [];
    }
    const parent = this.getItem(item.parent);
    if (!parent) {
      return [];
    }
    return [parent].concat(this.getAllParents(parent.id));
  }
}


const ts = new Tree(items);


let itemId = ts.getItem(8)
let item = document.getElementById('item')
 item.innerHTML = (
     'id: ' + itemId.id + ', parent:  ' + itemId.parent+ ', type:  ' + itemId.type + (`<br>`)
)


let allChildren = ts.getAllChildren(1)
let itemLinealChildren = document.getElementById('itemLinealChildren')
itemLinealChildren.innerHTML = (
    allChildren.map(allChildren => {
          return '{' + `${'id: ' + allChildren.id + ', parent: ' + allChildren.parent + ', type: ' + allChildren.type}` + '}' + (`<br>`)
    }
    )
)


let allParents = ts.getAllParents(7)
let itemAllParents = document.getElementById('itemAllParents')
itemAllParents.innerHTML = (
    allParents.map(allParents => {
          return '{' + `${'id: ' + allParents.id + ', parent: ' + allParents.parent + ', type: ' + allParents.type}` + '}' + (`<br>`)
        }
    )
)


let itemChildren = document.getElementById('itemChildren')
let children = ts.getChildren(1)
itemChildren.innerHTML = (
    children.map(children => {
          return '{' + `${'id: ' + children.id + ', parent: ' + children.parent + ', type: ' + children.type}` + '}' + (`<br>`)
        }
))


let allItems = document.getElementById('allItems')
allItems.innerHTML = (
    items.map(item => {
        return '{' + `${'id: ' + item.id + ', parent: ' + item.parent + ', type: ' + item.type}` + '}' + (`<br>`)
    })
)


