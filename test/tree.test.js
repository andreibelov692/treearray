const Tree = require("../Tree");

const items = [
  { id: 1, parent: "root", type: null },
  { id: 2, parent: 1, type: "test" },
  { id: 3, parent: 1, type: "test" },
  { id: 4, parent: 2, type: "test" },
  { id: 5, parent: 2, type: "test" },
  { id: 6, parent: 2, type: "test" },
  { id: 7, parent: 4, type: null },
  { id: 8, parent: 4, type: null },
];

const ts = new Tree(items);

describe("Tree", () => {
  it("should return all data", () => {
    expect(ts.getAll()).toEqual(items);
  });

  it("should return item with id 7", () => {
    expect(ts.getItem(7)).toEqual(items[6]);
  });

  it("should return children of item with id 4", () => {
    expect(ts.getChildren(4)).toEqual([items[6], items[7]]);
  });

  it("should return children of item with id 5", () => {
    expect(ts.getChildren(5)).toEqual([]);
  });

  it("should return children of item with id 2", () => {
    expect(ts.getChildren(2)).toEqual([items[3], items[4], items[5]]);
  });

  it("should return all children of item with id 2", () => {
    expect(ts.getAllChildren(2)).toEqual([
      items[3],
      items[4],
      items[5],
      items[6],
      items[7],
    ]);
  });

  it("should return all parents of item with id 7", () => {
    expect(ts.getAllParents(7)).toEqual([items[3], items[1], items[0]]);
  });
});
